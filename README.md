[![Build Status](https://codefirst.iut.uca.fr/api/badges/mohammad_zafir.jeeawody/serv_mobile/status.svg)](https://codefirst.iut.uca.fr/mohammad_zafir.jeeawody/serv_mobile)  
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=alert_status)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=bugs)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=code_smells)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=coverage)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)  
[![Duplicated Lines (%)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=duplicated_lines_density)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Lines of Code](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=ncloc)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=sqale_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=reliability_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)  
[![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=security_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Technical Debt](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=sqale_index)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=serv_mobile&metric=vulnerabilities)](https://codefirst.iut.uca.fr/sonar/dashboard?id=serv_mobile)  

 
# serv_mobile

Welcome on the serv_mobile project!  

  

_Generated with a_ **Code#0** _template_  
<img src="Documentation/doc_images/CodeFirst.png" height=40/>   